import 'dart:convert';
import 'dart:io';

import 'package:app/app/utils/uri.dart';
import 'package:flutter_file_downloader/flutter_file_downloader.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Http extends GetxService {
  final HttpClient _client = HttpClient();
  final SharedPreferences prefs;

  Http(this.prefs);

  Future<HttpResponseSuccess<T>> _request<T>(String path,
      {String method = 'GET', Map<String, String>? headers, dynamic body}) async {
    final uri = Uri.parse(baseUrl(path));
    final request = await _client.openUrl(method, uri);
    request.headers.contentType = ContentType.json;
    if (headers != null) {
      headers.forEach((key, value) {
        request.headers.add(key, value);
      });
    }
    request.headers.add('Accept', 'application/json');
    request.headers.add('Content-Type', 'application/json');
    final token = prefs.getString('token');
    if (token != null && token.isNotEmpty) {
      request.headers.add('Authorization', 'Bearer $token');
    }

    if (body != null) {
      request.add(utf8.encode(json.encode(body)));
    }
    final response = await request.close();
    final responseBody = await response.transform(utf8.decoder).join();

    if (response.statusCode == 200 ||
        response.statusCode == 201 ||
        response.statusCode == 204) {
      return HttpResponseSuccess<T>(json.decode(responseBody), response.statusCode);
    } else {
      final jsonR = json.decode(responseBody);
      throw HttpResponseFailure(jsonR['message'] ?? jsonR['error'] ?? 'Ocurrio un error',
          body: jsonR, statusCode: response.statusCode);
    }
  }

  Future<HttpResponseSuccess<T>> get<T>(String path,
      {Map<String, String>? headers}) async {
    return await _request<T>(path, method: 'GET', headers: headers);
  }

  Future<HttpResponseSuccess<T>> post<T>(String path,
      {Map<String, String>? headers, dynamic body}) async {
    return await _request<T>(path, method: 'POST', headers: headers, body: body);
  }

  Future<HttpResponseSuccess<T>> put<T>(String path,
      {Map<String, String>? headers, dynamic body}) async {
    return await _request<T>(path, method: 'PUT', headers: headers, body: body);
  }

  Future<HttpResponseSuccess<T>> delete<T>(String path,
      {Map<String, String>? headers}) async {
    return await _request<T>(path, method: 'DELETE', headers: headers);
  }

  Future<HttpResponseSuccess<T>> patch<T>(String path,
      {Map<String, String>? headers, dynamic body}) async {
    return await _request<T>(path, method: 'PATCH', headers: headers, body: body);
  }

  Future<void> download(String path) async {
    await FileDownloader.downloadFile(
      url: baseUrl(path),
      // onProgress: (s, d) => print('$s $d'),
      // onDownloadCompleted: (s) => print('path: $s'),
    );
  }
}

abstract class HttpResponse {
  final int? statusCode;

  HttpResponse(this.statusCode);
}

class HttpResponseSuccess<T> extends HttpResponse {
  final T data;

  HttpResponseSuccess(this.data, int? statusCode) : super(statusCode);

  bool isOk() => statusCode == 200 || statusCode == 201 || statusCode == 204;
}

class HttpResponseFailure extends HttpResponse {
  final String message;
  final Map<String, dynamic> body;

  HttpResponseFailure(this.message, {required this.body, required int statusCode})
      : super(statusCode);
}
