import 'package:app/data/repositories/mensualidades_repository.dart';
import 'package:app/domain/contracts/base_repository.dart';
import 'package:app/domain/models/mensualidad.dart';
import 'package:app/domain/models/payment.dart';

class PaymentsRepository extends BaseRepository<Payment> {
  PaymentsRepository(super.client, this.mensualidadesRepository);

  final MensualidadesRepository mensualidadesRepository;

  Future<List<Mensualidad>> getAll() async {
    return await mensualidadesRepository.getMensualidades();
  }

// Future<Mensualidad> get(int id) async {
//   final res = await client.get('/api/payments/$id');
//   return Payment.fromJson(res.data);
// }
}
