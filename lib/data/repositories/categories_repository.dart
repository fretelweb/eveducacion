import 'package:app/domain/contracts/base_repository.dart';
import 'package:app/domain/models/category.dart';

class CategoriesRepository extends BaseRepository<Category> {
  CategoriesRepository(super.client);

  Future<Category> get(int id) async {
    final res = await client.get('/api/categories/$id');
    return Category.fromJson(res.data);
  }

  Future<List<Category>> getAll({Map<String, dynamic>? queryParameters}) async {
    final res = await client.get<List<dynamic>>('/api/categories');
    return res.data.map((e) => Category.fromJson(e)).toList();
  }

  Future<List<Category>> getPostsByCategory(int categoryId) async {
    final res = await client.get<List<dynamic>>('/api/categories/$categoryId/posts');
    return res.data.map((e) => Category.fromJson(e)).toList();
  }
}
