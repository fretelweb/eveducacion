import 'package:app/data/http.dart';
import 'package:app/domain/models/mensualidad.dart';

class MensualidadesRepository {
  final Http cliente;

  MensualidadesRepository(this.cliente);

  Future<List<Mensualidad>> getMensualidades() async {
    final response = await cliente.get('/api/mensualidades');
    return response.data
        .map<Mensualidad>((e) => Mensualidad.fromJson(e))
        .toList();
  }
}
