import 'package:app/data/dto/login_request.dart';
import 'package:app/data/dto/login_response.dart';
import 'package:app/domain/contracts/base_repository.dart';
import 'package:app/domain/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthRepository extends BaseRepository {
  AuthRepository(super.client);

  Future<LoginResponse?> login(LoginRequest request) async {
    final response = await client.post('/api/login', body: request.toMap());
    if (!response.isOk()) return null;

    final prefs = await SharedPreferences.getInstance();
    await prefs.setString('token', response.data['access_token']);

    final user = await me();
    if (user == null) return null;

    return LoginResponse(
      accessToken: response.data['access_token'],
      tokenType: response.data['token_type'],
      user: user,
    );
  }

  Future<bool> isAuthenticated() async {
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString('token');
    if (token == null) return false;

    final user = await me();
    if (user == null) return false;

    return true;
  }

  Future<User?> me() async {
    try {
      final responseMe = await client.get('/api/me');
      if (!responseMe.isOk()) return null;
      return User.fromJson(responseMe.data);
    } catch (e) {
      return null;
    }
  }
}
