import 'package:app/domain/contracts/base_repository.dart';
import 'package:app/domain/models/app.dart';

class AppRepository extends BaseRepository<App> {
  AppRepository(super.client);

  Future<App> get(int id) async {
    final res = await client.get('/api/app');
    return App.fromJson(res.data);
  }
}
