import 'package:app/domain/contracts/base_repository.dart';
import 'package:app/domain/models/post.dart';

class PostsRepository extends BaseRepository<Post> {
  PostsRepository(super.client);

  Future<List<Post>> getPostsByCategory(int categoryId) async {
    final res = await client.get<List<dynamic>>('/api/categories/$categoryId/posts');
    return res.data.map((e) => Post.fromJson(e)).toList();
  }

  Future<Post> get(int postId) async {
    final res = await client.get('/api/posts/$postId');
    return Post.fromJson(res.data);
  }
}
