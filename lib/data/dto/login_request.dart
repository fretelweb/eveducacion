class LoginRequest {
  final String email;
  final String password;

  LoginRequest(this.email, this.password);

  toMap() => {'email': email, 'password': password};
}
