import 'package:app/domain/models/user.dart';

class LoginResponse {
  final String accessToken;
  final String tokenType;

  final User user;

  LoginResponse({
    required this.accessToken,
    required this.tokenType,
    required this.user,
  });
}
