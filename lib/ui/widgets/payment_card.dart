import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PaymentCard extends StatelessWidget {
  const PaymentCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _paymentCard();
  }

  Widget _paymentCard() {
    return Stack(children: [
      Container(
        height: 60,
        decoration: const BoxDecoration(color: Colors.red),
      ),
      Container(
        padding: const EdgeInsets.only(left: 10, right: 10),
        child: Container(
          width: double.infinity,
          padding: const EdgeInsets.all(10),
          decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(10)),
            boxShadow: [
              BoxShadow(
                color: Colors.black12,
                blurRadius: 5.0,
                spreadRadius: 1.0,
                offset: Offset(0.0, 0.0),
              ),
            ],
          ),
          child: Row(
            children: [
              Flexible(
                flex: 2,
                fit: FlexFit.tight,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: const [
                      Text('Pago mensual',
                          style: TextStyle(fontWeight: FontWeight.bold)),
                      SizedBox(height: 5),
                      Text(
                        '\$198.000.',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20),
                      ),
                      SizedBox(height: 5),
                      Text('Mes de pago: Agosto'),
                    ]),
              ),
              Flexible(
                flex: 1,
                child: Center(
                  child: ElevatedButton(
                      onPressed: () {
                        Get.showSnackbar(
                          const GetSnackBar(
                            title: 'Pago realizado',
                            message: 'Ok!',
                            duration: Duration(seconds: 5),
                            backgroundColor: Colors.red,
                          ),
                        );
                      },
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.green,
                        foregroundColor: Colors.white,
                        minimumSize: const Size.fromHeight(40),
                      ),
                      child: const Text('Pagar')),
                ),
              ),
            ],
          ),
        ),
      ),
    ]);
  }
}
