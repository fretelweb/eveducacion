import 'package:app/domain/models/user.dart';
import 'package:app/ui/pages/payments/payments_page.dart';
import 'package:app/ui/themes/theme_constants.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class StudentCard extends StatelessWidget {
  final User user;

  const StudentCard({Key? key, required this.user}) : super(key: key);

  get canPay => user.mensualidad != null && user.mensualidad!.pagado == false;

  @override
  Widget build(BuildContext context) {
    return _studentCard();
  }

  Widget _studentCard() {
    return Stack(children: [
      Container(
        height: 60,
        decoration: const BoxDecoration(color: colorPrimary),
      ),
      Container(
        padding: const EdgeInsets.only(left: 10, right: 10),
        child: Container(
          width: double.infinity,
          padding: const EdgeInsets.all(10),
          decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(10)),
            boxShadow: [
              BoxShadow(
                color: Colors.black12,
                blurRadius: 5.0,
                spreadRadius: 1.0,
                offset: Offset(0.0, 0.0),
              ),
            ],
          ),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Alumno: ${user.parvulo!.nombrecompleto}',
                    style: const TextStyle(fontWeight: FontWeight.bold)),
                const SizedBox(height: 5),
                Text('Nivel: ${user.parvulo!.nivelId}'),
                const SizedBox(height: 5),
                Text('Fecha Ingreso: ${(user.parvulo!.fechanacimiento)}'),
                const SizedBox(height: 5),
                Center(
                  child: canPay
                      ? ElevatedButton(
                          onPressed: () {
                            Get.toNamed(PaymentsPage.routeName);
                          },
                          style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.green,
                            foregroundColor: Colors.white,
                            // minimumSize: Size.fromHeight(40),
                          ),
                          child: const Text('Pago de mensualidad'))
                      : null,
                )
              ]),
        ),
      ),
    ]);
  }
}
