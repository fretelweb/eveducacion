import 'package:app/app/constants/app_constants.dart';
import 'package:app/domain/models/category.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class CategoryCard extends StatelessWidget {
  final Category category;
  final Widget? image;
  final VoidCallback? onTap;

  const CategoryCard({
    Key? key,
    required this.category,
    this.image,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Container(
        padding: const EdgeInsets.all(5),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: Colors.grey.shade300),
        ),
        child: ListTile(
          onTap: onTap,
          leading: image ??
              (category.foto != null && category.foto != ''
                  ? Image.network(
                      '${AppConstants.apiBaseUrl}/${category.foto}',
                      width: 60.0,
                    )
                  : const Icon(FontAwesomeIcons.image)),
          title: Text(category.nombre),
          trailing: const Icon(FontAwesomeIcons.chevronRight),
        ),
      ),
    );
  }
}
