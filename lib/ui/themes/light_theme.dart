import 'package:app/app/utils/color.dart';
import 'package:app/ui/themes/theme_constants.dart';
import 'package:flutter/material.dart';

final ThemeData lightTheme = ThemeData(
  primaryColor: colorPrimary,
  primarySwatch: getMaterialColor(colorPrimary),
  appBarTheme: const AppBarTheme(
    foregroundColor: Colors.white,
  ),
  navigationBarTheme: const NavigationBarThemeData(
    elevation: 0,
  ),
  scaffoldBackgroundColor: Colors.white,
);
