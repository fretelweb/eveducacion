import 'package:flutter/material.dart';

final ThemeData darkTheme = ThemeData(
  brightness: Brightness.dark,
  // accentIconTheme: IconThemeData(color: Colors.black),
  dividerColor: Colors.black12,
  colorScheme:
      ColorScheme.fromSwatch(primarySwatch: Colors.red).copyWith(secondary: Colors.red),
);
