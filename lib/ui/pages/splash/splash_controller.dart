import 'package:app/app/services/app_service.dart';
import 'package:app/app/utils/uri.dart';
import 'package:app/ui/pages/login/login_page.dart';
import 'package:app/ui/pages/tabs/tabs_page.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashController extends GetxController {
  final AppService appService;
  final SharedPreferences prefs;

  SplashController(this.appService, this.prefs);

  RxBool loading = true.obs;

  final RxString url = ''.obs;

  @override
  Future<void> onInit() async {
    super.onInit();
    await appService.init();
    url.value = baseUrl('${appService.app?.logo}');
    loading.value = false;
    Future.delayed(const Duration(seconds: 1), () async {
      if (appService.user == null) {
        prefs.clear();
        Get.offNamed(LoginPage.routeName);
      } else {
        Get.offNamed(TabsPage.routeName);
      }
    });
  }
}
