import 'package:app/ui/pages/splash/splash_controller.dart';
import 'package:app/ui/widgets/loader.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SplashPage extends GetView<SplashController> {
  static const routeName = '/splash';

  const SplashPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Obx(() => CachedNetworkImage(
              imageUrl: controller.url.value,
              errorWidget: (context, url, error) => const Loader(),
            )),
      ),
    );
  }
}
