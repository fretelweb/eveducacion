import 'package:app/ui/pages/login/login_controller.dart';
import 'package:app/ui/pages/login/login_service.dart';
import 'package:get/get.dart';

class LoginBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => LoginController(Get.find(), Get.find()));
    Get.lazyPut(() => LoginService(Get.find(), Get.find()));
  }
}