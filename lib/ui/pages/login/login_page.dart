import 'package:app/ui/pages/login/login_controller.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';

class LoginPage extends GetView<LoginController> {
  static const routeName = '/login';

  const LoginPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(28.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CachedNetworkImage(
                  placeholder: (_, __) => const CircularProgressIndicator(),
                  imageUrl: controller.appLogo,
                ),
                const SizedBox(height: 30),
                Form(
                  child: Column(
                    children: [
                      TextFormField(
                        onChanged: controller.setEmail,
                        decoration: const InputDecoration(
                          labelText: 'Correo electrónico',
                          hintText: 'Ingrese su correo electrónico',
                          border: OutlineInputBorder(),
                        ),
                        textInputAction: TextInputAction.next,
                        keyboardType: TextInputType.emailAddress,
                      ),
                      const SizedBox(height: 20),
                      TextFormField(
                        onChanged: controller.setPassword,
                        obscureText: true,
                        decoration: const InputDecoration(
                          labelText: 'Contraseña',
                          hintText: 'Ingrese su contraseña',
                          border: OutlineInputBorder(),
                        ),
                        textInputAction: TextInputAction.done,
                        keyboardType: TextInputType.visiblePassword,
                        onFieldSubmitted: (_) => controller.login(),
                      ),
                      const SizedBox(height: 20),
                      ElevatedButton(
                          onPressed: controller.login,
                          style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.orangeAccent,
                              foregroundColor: Colors.white,
                              minimumSize: const Size.fromHeight(50)),
                          child: const Text('Ingreso')),
                      const SizedBox(height: 20),
                      TextButton(
                        onPressed: () {
                          Get.toNamed('/reset-password');
                        },
                        child: const Text('¿Olvidaste tu contraseña?'),
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    IconButton(
                      onPressed: () => controller.openFacebook(),
                      icon: const FaIcon(FontAwesomeIcons.facebook),
                      color: Colors.brown,
                    ),
                    const SizedBox(width: 20),
                    IconButton(
                        onPressed: () => controller.openInstagram(),
                        icon: const FaIcon(FontAwesomeIcons.instagram),
                        color: Colors.brown),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
