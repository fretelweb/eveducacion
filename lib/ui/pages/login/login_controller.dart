import 'package:app/app/services/app_service.dart';
import 'package:app/app/utils/my_snackbar.dart';
import 'package:app/app/utils/uri.dart';
import 'package:app/data/http.dart';
import 'package:app/ui/pages/login/login_service.dart';
import 'package:app/ui/pages/tabs/tabs_page.dart';
import 'package:get/get.dart';

class LoginController extends GetxController {
  LoginController(this.service, this.appService);

  final LoginService service;
  final AppService appService;

  final email = ''.obs;
  final password = ''.obs;

  get app => service.app;

  get appLogo => baseUrl('${appService.app!.logo}');

  void setEmail(String value) => email.value = value;

  void setPassword(String value) => password.value = value;

  Future<void> login() async {
    try {
      final response = await service.login(email.value, password.value);
      final isSuccess = response != null;
      if (isSuccess) {
        appService.setToken(response.accessToken);
        appService.setUser(response.user);
        Get.offAllNamed(TabsPage.routeName);
      } else {
        Get.snackbar('Error', 'Failed to login');
      }
    } on HttpResponseFailure catch (e) {
      if (e.statusCode == 401) {
        mySnackBar('Error', 'Invalid credentials', type: MySnackBarType.error);
      } else {
        mySnackBar('Error', e.message, type: MySnackBarType.error);
      }
    } catch (e) {
      mySnackBar('Error', 'Failed to login', type: MySnackBarType.error);
    }
  }

  openFacebook() async {
    await appService.launchFacebook();
  }

  openInstagram() async {
    await appService.launchInstagram();
  }
}
