import 'package:app/app/services/app_service.dart';
import 'package:app/data/dto/login_request.dart';
import 'package:app/data/dto/login_response.dart';
import 'package:app/domain/models/user.dart';
import 'package:app/data/repositories/auth_repository.dart';
import 'package:app/data/http.dart';
import 'package:url_launcher/url_launcher_string.dart';

class LoginService {
  LoginService(this.authRepo, this.appService);

  final AuthRepository authRepo;
  final AppService appService;

  get app => appService.app;

  Future<bool> isAuthenticated() async {
    try {
      return await authRepo.isAuthenticated();
    } on HttpResponseFailure {
      return false;
    }
  }

  Future<User?> me() async {
    return await authRepo.me();
  }

  Future<LoginResponse?> login(String email, String password) async {
    var response = await authRepo.login(
      LoginRequest(email, password),
    );
    return response;
  }

  Future<void> launchFacebook() async {
    if (await canLaunchUrlString('fb://page/${app.facebook}')) {
      launchUrlString('fb://page/${app.facebook}');
    } else {
      launchUrlString('https://www.facebook.com/${app.facebook}');
    }
  }

  Future<void> launchInstagram() async {
    if (await canLaunchUrlString('instagram://user?username=${app.instagram}')) {
      launchUrlString('instagram://user?username=${app.instagram}');
    } else {
      launchUrlString('https://www.instagram.com/${app.instagram}');
    }
  }
}
