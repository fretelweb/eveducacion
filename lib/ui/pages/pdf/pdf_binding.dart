import 'package:app/ui/pages/pdf/pdf_controller.dart';
import 'package:get/get.dart';

class PdfBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => PdfController());
  }
}