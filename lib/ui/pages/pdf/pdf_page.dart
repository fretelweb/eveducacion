import 'package:app/ui/pages/pdf/pdf_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:internet_file/internet_file.dart';
import 'package:pdfx/pdfx.dart' as pdfx;

class PdfPage extends GetView<PdfController> {
  static const routeName = '/pdf';

  const PdfPage({super.key});

  @override
  Widget build(BuildContext context) {
    final url = Get.arguments['url'];
    final document = pdfx.PdfDocument.openData(InternetFile.get(url));
    return Scaffold(
      appBar: AppBar(),
      body: pdfx.PdfView(
        controller: pdfx.PdfController(document: document),
      ),
    );
  }
}
