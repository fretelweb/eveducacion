import 'package:app/app/services/app_service.dart';
import 'package:app/domain/models/mensualidad.dart';
import 'package:app/domain/models/user.dart';
import 'package:app/ui/pages/payment/payment_page.dart';
import 'package:app/ui/pages/payments/payments_service.dart';
import 'package:get/get.dart';

class PaymentsController extends GetxController {
  PaymentsController(this.service, this.appService);

  final AppService appService;
  final PaymentsService service;
  final RxBool _loading = true.obs;
  final RxList<Mensualidad> _mensualidades = <Mensualidad>[].obs;

  bool get loading => _loading.value;

  List<Mensualidad> get mensualidades => _mensualidades;

  User get user => appService.user!;

  @override
  Future<void> onInit() async {
    super.onInit();
    await loadPayments();
  }

  Future<void> loadPayments() async {
    _loading.value = true;
    _mensualidades.value = await service.getPayments();
    _loading.value = false;
  }

  pagar(Mensualidad mensualidad) {
    Get.toNamed(PaymentPage.routeName, arguments: mensualidad)!
        .then((value) async => await loadPayments());
  }
}
