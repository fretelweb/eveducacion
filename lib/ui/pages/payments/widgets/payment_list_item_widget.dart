import 'package:app/domain/models/mensualidad.dart';
import 'package:flutter/material.dart';

class PaymentListItemWidget extends StatelessWidget {
  PaymentListItemWidget({
    Key? key,
    required this.payment,
    required this.onTap,
  }) : super(key: key);

  final Map<String, List<Color>> colors = {
    "FUTURO": <Color>[Colors.grey.shade300, Colors.grey.shade900],
    "ACTUAL": <Color>[Colors.blue, Colors.green.shade50],
  };

  final Mensualidad payment;
  final Function(Mensualidad) onTap;

  get isPagado => payment.pagado;

  get isVencido => !payment.mesActual && !payment.mesFuturo && !payment.pagado;

  get isFuturo => payment.mesFuturo;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: const Text('Pago mensual'),
      subtitle: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            '\$${payment.valor}',
            style: const TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 18.0,
                color: Colors.black),
          ),
          Text(
            'Mes de pago: ${payment.nombreMes}',
            style: const TextStyle(fontSize: 11),
          )
        ],
      ),
      trailing: _buildPaymentButton(),
    );
  }

  Widget _buildPaymentButton() {
    if (isVencido) {
      return const Text('VENCIDO', style: TextStyle(color: Colors.red));
    }
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        backgroundColor: isFuturo ? colors['FUTURO']![0] : colors['ACTUAL']![0],
        foregroundColor: isFuturo ? colors['FUTURO']![1] : colors['ACTUAL']![1],
      ),
      onPressed: !isPagado ? () => onTap(payment) : null,
      child: Text(isPagado ? 'PAGADO' : 'PAGAR'),
    );
  }
}
