import 'package:app/ui/pages/payments/payments_controller.dart';
import 'package:app/ui/pages/payments/payments_service.dart';
import 'package:get/get.dart';

class PaymentsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => PaymentsController(Get.find(), Get.find()));
    Get.lazyPut(() => PaymentsService(Get.find()));
  }
}
