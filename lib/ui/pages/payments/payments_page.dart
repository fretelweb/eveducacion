import 'package:app/ui/pages/payments/payments_controller.dart';
import 'package:app/ui/pages/payments/widgets/payment_list_item_widget.dart';
import 'package:app/ui/widgets/loader.dart';
import 'package:app/ui/widgets/student_card.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PaymentsPage extends GetView<PaymentsController> {
  static const routeName = '/payments';

  const PaymentsPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(elevation: 0),
      body: SafeArea(
        child: Obx(() {
          if (controller.loading) return const Loader();

          return Column(
            children: [
              StudentCard(user: controller.user),
              Expanded(
                child: ListView.builder(
                  itemCount: controller.mensualidades.length,
                  itemBuilder: (context, index) {
                    final mensualidad = controller.mensualidades[index];
                    return PaymentListItemWidget(
                      payment: mensualidad,
                      onTap: controller.pagar,
                    );
                  },
                ),
              )
            ],
          );
        }),
      ),
    );
  }
}
