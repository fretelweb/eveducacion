import 'package:app/data/repositories/payments_repository.dart';
import 'package:app/domain/models/mensualidad.dart';
import 'package:get/get.dart';

class PaymentsService extends GetxService {
  final PaymentsRepository paymentsRepository;

  PaymentsService(this.paymentsRepository);

  Future<List<Mensualidad>> getPayments() async {
    return await paymentsRepository.getAll();
  }
}
