import 'dart:io';

import 'package:app/ui/pages/payment/payment_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:get/get.dart';

class PaymentPage extends GetView<PaymentController> {
  static const routeName = '/payment';

  const PaymentPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Registrar Pagos')),
      body: InAppWebView(
        initialUrlRequest: URLRequest(url: Uri.parse(controller.urlPayment)),
        onLoadStop: (InAppWebViewController wbController, Uri? url) async {
          print('NAVIGATE TO $url');
          if (controller.urlSuccess == url.toString()) {
            print('NAVIGATE TO SUCCESS $url');
            sleep(const Duration(seconds: 3));
            Get.back();
          }
        },
      ),
    );
  }
}
