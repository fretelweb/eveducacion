import 'package:app/app/services/app_service.dart';
import 'package:app/domain/models/mensualidad.dart';
import 'package:get/get.dart';

class PaymentController extends GetxController {
  PaymentController(this.appService);

  final AppService appService;

  final String urlSuccess =
      'https://eveducacion.enlacevisual.cl/sunshine/payments/online/success';
  final String _urlPayment = 'https://eveducacion.enlacevisual'
      '.cl/sunshine/payments/online';

  final Mensualidad mensualidad = Get.arguments;

  get urlPayment =>
      '$_urlPayment?parvulo=${appService.user!.parvuloId}&mes=${mensualidad.id}';
}
