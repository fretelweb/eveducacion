import 'package:app/ui/pages/category/category_controller.dart';
import 'package:app/ui/widgets/student_card.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CategoryPage extends GetView<CategoryController> {
  static const routeName = '/category';

  const CategoryPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Obx(() => Text('Category ${controller.category.nombre}')),
        elevation: 0,
      ),
      body: Column(
        children: [
          StudentCard(user: controller.user),
          Expanded(
            child: Center(
              child: Obx(() => GridView.builder(
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    childAspectRatio: 1.6,
                  ),
                  itemCount: controller.posts.length,
                  itemBuilder: (context, index) {
                    final post = controller.posts[index];
                    return Card(
                      child: Center(
                        child: ListTile(
                          onTap: () => controller.postDetails(post),
                          title: Text(post.titulo),
                          subtitle: Text(post.descripcion),
                        ),
                      ),
                    );
                  })),
            ),
          ),
        ],
      ),
    );
  }
}
