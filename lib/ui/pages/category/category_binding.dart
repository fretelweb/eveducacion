import 'package:app/ui/pages/category/category_controller.dart';
import 'package:app/ui/pages/category/category_service.dart';
import 'package:get/get.dart';

class CategoryBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => CategoryController(Get.find(), Get.find()));
    Get.lazyPut(() => CategoryService(Get.find(), Get.find()));
  }
}