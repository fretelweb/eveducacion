import 'package:app/domain/models/category.dart';
import 'package:app/domain/models/post.dart';
import 'package:app/data/repositories/categories_repository.dart';
import 'package:app/data/repositories/posts_repository.dart';
import 'package:get/get.dart';

class CategoryService extends GetxService {
  final CategoriesRepository categoryRepo;
  final PostsRepository postRepo;

  CategoryService(this.categoryRepo, this.postRepo);

  Future<List<Category>> getCategories() async {
    return await categoryRepo.getAll();
  }

  Future<Category> getCategory(int categoryId) async {
    return await categoryRepo.get(categoryId);
  }

  Future<List<Post>> getPostsByCategory(int categoryId) async {
    return await postRepo.getPostsByCategory(categoryId);
  }

  Future<Post> getPost(int postId) async {
    return await postRepo.get(postId);
  }
}
