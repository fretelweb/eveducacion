import 'package:app/app/services/app_service.dart';
import 'package:app/domain/models/category.dart';
import 'package:app/domain/models/post.dart';
import 'package:app/domain/models/user.dart';
import 'package:app/ui/pages/category/category_service.dart';
import 'package:app/ui/pages/post/post_page.dart';
import 'package:get/get.dart';

class CategoryController extends GetxController {
  CategoryController(this.service, this.appService);

  final CategoryService service;
  final AppService appService;

  final Rx<Category> _category = Category(nombre: '', publicar: '').obs;
  final RxList<Post> _posts = <Post>[].obs;

  Category get category => _category.value;

  List<Post> get posts => _posts.toList();

  User get user => appService.user!;

  @override
  Future<void> onInit() async {
    final category = Get.arguments as Category?;
    if (category == null || category.id == null) {
      Get.back();
      return;
    }
    try {
      _posts.value = await service.getPostsByCategory(category.id!);
      _category.value = category;
    } catch (e) {
      Get.snackbar('Error', e.toString());
    }
    super.onInit();
  }

  void postDetails(Post post) {
    Get.toNamed(PostPage.routeName, arguments: {'post': post});
  }
}
