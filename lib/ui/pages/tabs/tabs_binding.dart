import 'package:app/ui/pages/tabs/home/home_controller.dart';
import 'package:app/ui/pages/tabs/home/home_service.dart';
import 'package:app/ui/pages/tabs/profile/profile_controller.dart';
import 'package:app/ui/pages/tabs/tabs_controller.dart';
import 'package:get/get.dart';

class TabsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => HomeService(Get.find()));
    Get.lazyPut(() => HomeController(Get.find(), Get.find()));
    Get.lazyPut(() => ProfileController(Get.find()));
    Get.lazyPut(() => TabsController(Get.find()));
  }
}
