import 'package:app/app/services/app_service.dart';
import 'package:app/ui/pages/login/login_page.dart';
import 'package:get/get.dart';

class ProfileController extends GetxController {
  ProfileController(this.appService);

  final AppService appService;

  logout() async {
    appService.cleanToken();
    appService.setUser(null);
    Get.offAllNamed(LoginPage.routeName);
  }
}
