import 'package:app/app/services/app_service.dart';
import 'package:app/domain/models/category.dart';
import 'package:app/domain/models/user.dart';
import 'package:app/ui/pages/tabs/home/home_service.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {
  HomeController(this.appService, this.service);

  final AppService appService;
  final HomeService service;

  final RxList<Category> categories = <Category>[].obs;
  final RxBool loading = true.obs;

  Rx<User> user = User(name: '', email: '').obs;

  @override
  Future<void> onInit() async {
    super.onInit();
    user.value = appService.user!;
    categories.value = await service.getCategories();
    loading.value = false;
  }
}
