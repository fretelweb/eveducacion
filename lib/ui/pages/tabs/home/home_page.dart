import 'package:app/domain/models/category.dart';
import 'package:app/ui/pages/category/category_page.dart';
import 'package:app/ui/pages/payments/payments_page.dart';
import 'package:app/ui/pages/tabs/home/home_controller.dart';
import 'package:app/ui/widgets/category_card.dart';
import 'package:app/ui/widgets/student_card.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';

class HomePage extends GetView<HomeController> {
  static const routeName = '/home';

  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          titleSpacing: 0,
          title: Obx(() => Text('Hola ${controller.user.value.name}')),
          leading: Obx(() => Padding(
                padding: const EdgeInsets.symmetric(vertical: 10.0),
                child: CircleAvatar(
                  backgroundColor: Colors.white38,
                  foregroundColor: Colors.white,
                  child: Text(controller.user.value.name[0]),
                ),
              )),
          shadowColor: Colors.transparent,
        ),
        body: Column(
          children: [
            Obx(() => StudentCard(user: controller.user.value)),
            Expanded(
              child: Obx(() => ListView.builder(
                  itemCount: controller.categories.length + 1,
                  itemBuilder: (context, index) {
                    if (index < controller.categories.length) {
                      final category = controller.categories[index];
                      return CategoryCard(
                        category: category,
                        onTap: () => Get.toNamed(
                          CategoryPage.routeName,
                          arguments: category,
                        ),
                      );
                    }
                    return CategoryCard(
                      onTap: () => Get.toNamed(PaymentsPage.routeName),
                      category: Category(
                        id: 0,
                        nombre: 'Gestión de Pagos',
                        publicar: 'NO',
                        foto: '',
                      ),
                      image: const Icon(
                        FontAwesomeIcons.ccVisa,
                        size: 40,
                        color: Colors.lightGreen,
                      ),
                    );
                  })),
            ),
          ],
        ));
  }
}
