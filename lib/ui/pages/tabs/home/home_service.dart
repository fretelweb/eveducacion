import 'package:app/domain/models/category.dart';
import 'package:app/data/repositories/categories_repository.dart';

class HomeService {
  final CategoriesRepository categoryRepo;

  HomeService(this.categoryRepo);

  Future<List<Category>> getCategories() async {
    return await categoryRepo.getAll();
  }
}
