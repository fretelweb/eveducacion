import 'package:app/app/services/app_service.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher_string.dart';

class TabsController extends GetxController {
  TabsController(this.appService);

  final AppService appService;

  get app => appService.app;

  final currentIndex = 0.obs;

  @override
  void onInit() {
    print('ON INIT LOGIN CONTROLLER');
    super.onInit();
  }

  Future<void> changeTabIndex(int index) async {
    if (index == 0 || index == 4) {
      currentIndex.value = index;
    }
    if (index == 1) {
      // Get.toNamed('/contacto');
      launchUrlString('mailto:${app.email}');
    }

    if (index == 2) {
      appService.launchFacebook();
    }

    if (index == 3) {
      appService.launchInstagram();
    }
  }
}
