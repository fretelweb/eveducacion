import 'package:app/domain/models/post.dart';
import 'package:app/data/repositories/posts_repository.dart';

class PostService {
  final PostsRepository postRepository;

  PostService(this.postRepository);

  Future<Post> getPost(int id) async {
    return await postRepository.get(id);
  }
}
