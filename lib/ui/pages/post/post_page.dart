import 'package:app/ui/pages/post/post_controller.dart';
import 'package:app/ui/widgets/student_card.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PostPage extends GetView<PostController> {
  static const routeName = '/post';

  const PostPage({super.key});

  final titleStyle = const TextStyle(fontSize: 18, fontWeight: FontWeight.bold);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Obx(() => Text('Post ${controller.post.titulo}')),
          elevation: 0,
        ),
        body: Column(
          children: [
            StudentCard(user: controller.user),
            Obx(() {
              return Padding(
                padding: const EdgeInsets.all(15.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      controller.post.titulo,
                      style: titleStyle,
                    ),
                    const SizedBox(height: 15),
                    Text('Descripción: ', style: titleStyle),
                    const SizedBox(height: 10),
                    Text(controller.post.descripcion),
                    const SizedBox(height: 15),
                    Text('Archivo: ', style: titleStyle),
                    const SizedBox(height: 10),
                    Text(controller.post.archivo),
                    const SizedBox(height: 15),
                    ElevatedButton(
                      onPressed: () {
                        // print(controller.post.archivo);
                        controller.downloadFile(controller.post.archivo);
                      },
                      style: ElevatedButton.styleFrom(
                        minimumSize: const Size.fromHeight(40),
                      ),
                      child: const Text('Ver archivo'),
                    )
                  ],
                ),
              );
            }),
          ],
        ),
      ),
    );
  }
}
