import 'package:app/app/services/app_service.dart';
import 'package:app/app/utils/uri.dart';
import 'package:app/domain/models/post.dart';
import 'package:app/ui/pages/pdf/pdf_page.dart';
import 'package:app/ui/pages/post/post_service.dart';
import 'package:get/get.dart';

class PostController extends GetxController {
  PostController(this.service, this.appService);

  final PostService service;
  final AppService appService;

  final Rx<Post> _post =
      Post(titulo: '', descripcion: '', archivo: '', publicar: '').obs;

  get post => _post.value;

  get user => appService.user;

  @override
  Future<void> onInit() async {
    final post = Get.arguments['post'] as Post;
    _post.value = await service.getPost(post.id!);
    super.onInit();
  }

  void downloadFile(String fileUrl) {
    fileUrl = baseUrl(fileUrl);
    Get.toNamed(PdfPage.routeName, arguments: {'url': fileUrl});
  }
}
