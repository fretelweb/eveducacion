import 'package:app/ui/pages/post/post_controller.dart';
import 'package:app/ui/pages/post/post_service.dart';
import 'package:get/get.dart';

class PostBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => PostController(Get.find(), Get.find()));
    Get.lazyPut(() => PostService(Get.find()));
  }
}