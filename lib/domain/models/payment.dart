import 'package:app/domain/models/anio.dart';
import 'package:app/domain/models/medio_pago.dart';
import 'package:app/domain/models/parvulo.dart';

class Payment {
  final int? id;
  final int? parvuloId;
  final int? medioPagoId;
  final int? anoId;
  final String comprobante;
  final int? orderNumber;
  final DateTime fechaPago;
  final double total;
  final Anio? anio;
  final Parvulo? parvulo;
  final MedioPago? medioPago;

  Payment({
    this.id,
    this.parvuloId,
    this.medioPagoId,
    this.anoId,
    this.orderNumber,
    this.anio,
    this.parvulo,
    this.medioPago,
    required this.comprobante,
    required this.fechaPago,
    required this.total,
  });

  static Payment fromJson(Map json) => Payment(
        id: json['id'],
        parvuloId: json['parvulo_id'],
        medioPagoId: json['medio_pago_id'],
        anoId: json['ano_id'],
        orderNumber: json['order_number'],
        comprobante: json['comprobante'],
        fechaPago: DateTime.parse(json['fecha_pago']),
        total: json['total'],
        anio: json['anio'] != null ? Anio.fromJson(json['anio']) : null,
        parvulo:
            json['parvulo'] != null ? Parvulo.fromJson(json['parvulo']) : null,
        medioPago: json['medio_pago'] != null
            ? MedioPago.fromJson(json['medio_pago'])
            : null,
      );
}
