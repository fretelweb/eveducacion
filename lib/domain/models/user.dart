import 'package:app/domain/models/mensualidad.dart';
import 'package:app/domain/models/parvulo.dart';

class User {
  final int? id; // uuid
  final String name;
  final String email;
  final int? parvuloId;
  final Parvulo? parvulo;
  final Mensualidad? mensualidad;

  User(
      {this.id,
      required this.name,
      required this.email,
      this.parvuloId,
      this.parvulo,
      this.mensualidad});

  factory User.fromJson(Map<String, dynamic> json) {
    print(json);
    return User(
      id: json['id'],
      name: json['name'],
      email: json['email'],
      parvuloId: json['parvulo_id'],
      parvulo:
          json['parvulo'] != null ? Parvulo.fromJson(json['parvulo']) : null,
      mensualidad: json['mensualidad'] != null
          ? Mensualidad.fromJson(json['mensualidad'])
          : null,
    );
  }

  User copyWith(
          {int? id,
          String? name,
          String? email,
          int? parvuloId,
          Parvulo? parvulo,
          Mensualidad? mensualidad}) =>
      User(
        id: id ?? this.id,
        name: name ?? this.name,
        email: email ?? this.email,
        parvuloId: parvuloId ?? this.parvuloId,
        parvulo: parvulo ?? this.parvulo,
        mensualidad: mensualidad ?? this.mensualidad,
      );

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'email': email,
      'parvulo_id': parvuloId,
    };
  }
}
