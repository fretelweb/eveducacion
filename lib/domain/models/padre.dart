class Padre{
  final String rut;
  final String nombre;
  final String telefono;
  final String email;
  final String direccion;

  Padre({required this.rut, required this.nombre, required this.telefono, required this.email, required this.direccion});

  Padre.fromJson(Map<String, dynamic> json)
      : rut = json['rut'],
        nombre = json['nombre'],
        telefono = json['telefono'],
        email = json['email'],
        direccion = json['direccion'];

  Padre copyWith({String? rut, String? nombre, String? telefono, String? email, String? direccion}) {
    return Padre(
      rut: rut ?? this.rut,
      nombre: nombre ?? this.nombre,
      telefono: telefono ?? this.telefono,
      email: email ?? this.email,
      direccion: direccion ?? this.direccion,
    );
  }
}