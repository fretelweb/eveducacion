class Post {
  final int? id; //: 1,
  final int? categoriaId; //: 1,
  final String titulo; //: "Reglamento Interno",
  final String descripcion; //: "relgmaento interno",
  final String archivo; //: "uploads\/1\/2023-03\/celebraciones_2023_1.docx",
  final String publicar; //: "SI",
  final DateTime? createdAt;
  final DateTime? updatedAt;
  final DateTime? deletedAt;

  Post(
      {this.id,
      this.categoriaId,
      required this.titulo,
      required this.descripcion,
      required this.archivo,
      required this.publicar,
      this.createdAt,
      this.updatedAt,
      this.deletedAt});

  factory Post.fromJson(Map<String, dynamic> json) {
    return Post(
      id: json['id'],
      categoriaId: json['categoria_id'],
      titulo: json['titulo'],
      descripcion: json['descripcion'],
      archivo: json['archivo'],
      publicar: json['publicar'],
      createdAt: DateTime.parse(json['created_at']),
      updatedAt: json['updated_at'] != null
          ? DateTime.parse(json['updated_at'])
          : null,
      deletedAt: json['deleted_at'] != null
          ? DateTime.parse(json['deleted_at'])
          : null,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'categoria_id': categoriaId,
      'titulo': titulo,
      'descripcion': descripcion,
      'archivo': archivo,
      'publicar': publicar,
      'created_at': createdAt,
      'updated_at': updatedAt,
      'deleted_at': deletedAt,
    };
  }

  Post copyWith({
    int? id,
    int? categoriaId,
    String? titulo,
    String? descripcion,
    String? archivo,
    String? publicar,
    DateTime? createdAt,
    DateTime? updatedAt,
    DateTime? deletedAt,
  }) {
    return Post(
      id: id ?? this.id,
      categoriaId: categoriaId ?? this.categoriaId,
      titulo: titulo ?? this.titulo,
      descripcion: descripcion ?? this.descripcion,
      archivo: archivo ?? this.archivo,
      publicar: publicar ?? this.publicar,
      createdAt: createdAt ?? this.createdAt,
      updatedAt: updatedAt ?? this.updatedAt,
      deletedAt: deletedAt ?? this.deletedAt,
    );
  }
}
