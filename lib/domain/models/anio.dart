class Anio {
  int? id;
  String ano;

  Anio({this.id, required this.ano});

  static fromJson(Map<String, dynamic> json) => Anio(
        id: json['id'],
        ano: json['ano'],
      );
}
