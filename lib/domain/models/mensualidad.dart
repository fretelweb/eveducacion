class Mensualidad {
  final int id; //: 9,
  final int anoId; //: 1,
  final DateTime createdAt; //: "2023-03-09T22:47:02.000000Z",
  final DateTime? updatedAt; //: "2023-04-15T18:02:48.000000Z",
  final String nombreMes; //: "NOVIEMBRE",
  final int valor; //: 190000,
  final int mes; //: 11,
  final DateTime? deletedAt; //: null,
  final bool pagado; //: false,
  final bool mesActual; //: false,
  final bool mesFuturo;

  Mensualidad({
    required this.id,
    required this.anoId,
    required this.createdAt,
    required this.updatedAt,
    required this.nombreMes,
    required this.valor,
    required this.mes,
    required this.deletedAt,
    required this.pagado,
    required this.mesActual,
    required this.mesFuturo,
  });

  factory Mensualidad.fromJson(Map<String, dynamic> json) => Mensualidad(
        id: json['id'],
        anoId: json['ano_id'],
        nombreMes: json['nombre_mes'],
        valor: json['valor'],
        mes: json['mes'],
        pagado: json['pagado'],
        mesActual: json['mes_actual'],
        mesFuturo: json['mes_futuro'],
        createdAt: DateTime.parse(json['created_at']),
        updatedAt: json['updated_at'] != null ? DateTime.parse(json['updated_at']) : null,
        deletedAt: json['deleted_at'] != null ? DateTime.parse(json['deleted_at']) : null,
      );
}
