import 'package:app/domain/models/padre.dart';

class Parvulo {
  final String rut;
  final String nombrecompleto;
  final String sexo;
  final String direccion;
  final String fechanacimiento;
  final String urgencia;
  final String prevision;
  final int nivelId;
  final int jornadaId;

  final Padre padre1;
  final Padre padre2;

  Parvulo(
      {required this.rut,
      required this.nombrecompleto,
      required this.sexo,
      required this.direccion,
      required this.fechanacimiento,
      required this.urgencia,
      required this.prevision,
      required this.nivelId,
      required this.jornadaId,
      required this.padre1,
      required this.padre2});

  factory Parvulo.fromJson(Map<String, dynamic> json) => Parvulo(
        rut: json['rut'],
        nombrecompleto: json['nombrecompleto'],
        sexo: json['sexo'],
        direccion: json['direccion'],
        fechanacimiento: json['fechanacimiento'],
        urgencia: json['urgencia'],
        prevision: json['prevision'],
        nivelId: json['nivel_id'],
        jornadaId: json['jornada_id'],
        padre1: Padre(
            rut: json['rutpadreuno'],
            nombre: json['nombrepadreuno'],
            telefono: json['telefonopadreuno'],
            email: json['emailpadreuno'],
            direccion: json['direccionpadreuno']),
        padre2: Padre(
            rut: json['rutpadredos'],
            nombre: json['nombrepadredos'],
            telefono: json['telefonopadredos'],
            email: json['emailpadredos'],
            direccion: json['direccionpadredos']),
      );

  Parvulo copyWith({
    String? rut,
    String? nombrecompleto,
    String? sexo,
    String? direccion,
    String? fechanacimiento,
    String? urgencia,
    String? prevision,
    int? nivel_id,
    int? jornada_id,
    Padre? padre1,
    Padre? padre2,
  }) =>
      Parvulo(
        rut: rut ?? this.rut,
        nombrecompleto: nombrecompleto ?? this.nombrecompleto,
        sexo: sexo ?? this.sexo,
        direccion: direccion ?? this.direccion,
        fechanacimiento: fechanacimiento ?? this.fechanacimiento,
        urgencia: urgencia ?? this.urgencia,
        prevision: prevision ?? this.prevision,
        nivelId: nivel_id ?? this.nivelId,
        jornadaId: jornada_id ?? this.jornadaId,
        padre1: padre1 ?? this.padre1,
        padre2: padre2 ?? this.padre2,
      );

  toMap() => {
        'rut': rut,
        'nombrecompleto': nombrecompleto,
        'sexo': sexo,
        'direccion': direccion,
        'fechanacimiento': fechanacimiento,
        'urgencia': urgencia,
        'prevision': prevision,
        'nivel_id': nivelId,
        'jornada_id': jornadaId,
      };
}
