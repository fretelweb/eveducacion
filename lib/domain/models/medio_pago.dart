class MedioPago {
  int? id;
  String nombre;

  MedioPago({this.id, required this.nombre});

  static fromJson(Map<String, dynamic> json) =>
      MedioPago(
        id: json['id'],
        nombre: json['nombre'],
      );
}
