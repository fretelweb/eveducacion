import 'package:app/data/http.dart';

abstract class BaseRepository<T> {
  final Http client;

  BaseRepository(this.client);
}
