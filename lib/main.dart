import 'package:app/app/bindings/initial_binding.dart';
import 'package:app/app/routes/app_routes.dart';
import 'package:app/ui/themes/light_theme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Get.putAsync(() => SharedPreferences.getInstance());
  // await GetStorage.init();

  runApp(GetMaterialApp(
    title: 'EV Educación',
    debugShowCheckedModeBanner: false,
    initialRoute: AppRoutes.initialPath,
    theme: lightTheme,
    // darkTheme: darkTheme,
    defaultTransition: Transition.native,
    getPages: AppRoutes.routes,
    initialBinding: InitialBinding(),
  ));
}
