import 'package:app/ui/pages/login/login_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class AuthMiddleware extends GetMiddleware {
  @override
  RouteSettings? redirect(String? route) {
    final box = GetStorage();
    bool isLogged = box.read('isLogged') ?? false;
    if (!isLogged && route != LoginPage.routeName) {
      return const RouteSettings(name: LoginPage.routeName);
    }
    return null;
  }
}
