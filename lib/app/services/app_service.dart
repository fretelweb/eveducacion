import 'package:app/domain/models/app.dart';
import 'package:app/domain/models/user.dart';
import 'package:app/data/repositories/app_repository.dart';
import 'package:app/data/repositories/auth_repository.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher_string.dart';

class AppService extends GetxService {
  AppService(this.appRepository, this.authRepository, this.prefs);

  final SharedPreferences prefs;
  final AppRepository appRepository;
  final AuthRepository authRepository;

  User? user;
  App? app;

  Future<void> init() async {
    app ??= await appRepository.get(1);
    user ??= await authRepository.me();
  }

  Future<void> launchFacebook() async {
    if (app == null) return;
    if (await canLaunchUrlString('fb://page/${app!.facebook}')) {
      launchUrlString('fb://page/${app!.facebook}');
    } else {
      launchUrlString('https://www.facebook.com/${app!.facebook}');
    }
  }

  Future<void> launchInstagram() async {
    if (app == null) return;
    if (await canLaunchUrlString(
        'instagram://user?username=${app!.instagram}')) {
      launchUrlString('instagram://user?username=${app!.instagram}');
    } else {
      launchUrlString('https://www.instagram.com/${app!.instagram}');
    }
  }

  void setUser(User? user) {
    this.user = user;
  }

  void setToken(String accessToken) {
    prefs.setString('token', accessToken);
  }

  String getToken() {
    return prefs.getString('token') ?? '';
  }

  void cleanToken() {
    prefs.remove('token');
  }
}
