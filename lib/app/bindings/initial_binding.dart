import 'package:app/app/services/app_service.dart';
import 'package:app/data/http.dart';
import 'package:app/data/repositories/app_repository.dart';
import 'package:app/data/repositories/auth_repository.dart';
import 'package:app/data/repositories/categories_repository.dart';
import 'package:app/data/repositories/mensualidades_repository.dart';
import 'package:app/data/repositories/payments_repository.dart';
import 'package:app/data/repositories/posts_repository.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class InitialBinding implements Bindings {
  @override
  void dependencies() {
    Get.putAsync<SharedPreferences>(
        () async => await SharedPreferences.getInstance());
    Get.lazyPut(() => Http(Get.find()));
    Get.lazyPut(() => AppService(Get.find(), Get.find(), Get.find()),
        fenix: true);

    Get.lazyPut(() => AppRepository(Get.find()), fenix: true);
    Get.lazyPut(() => AuthRepository(Get.find()), fenix: true);
    Get.lazyPut(() => CategoriesRepository(Get.find()), fenix: true);
    Get.lazyPut(() => PaymentsRepository(Get.find(), Get.find()), fenix: true);
    Get.lazyPut(() => PostsRepository(Get.find()), fenix: true);
    Get.lazyPut(() => MensualidadesRepository(Get.find()), fenix: true);
  }
}
