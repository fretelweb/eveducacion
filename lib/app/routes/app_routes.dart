import 'package:app/ui/pages/category/category_binding.dart';
import 'package:app/ui/pages/category/category_page.dart';
import 'package:app/ui/pages/login/login_binding.dart';
import 'package:app/ui/pages/login/login_page.dart';
import 'package:app/ui/pages/payment/payment_binding.dart';
import 'package:app/ui/pages/payment/payment_page.dart';
import 'package:app/ui/pages/payments/payments_binding.dart';
import 'package:app/ui/pages/payments/payments_page.dart';
import 'package:app/ui/pages/pdf/pdf_binding.dart';
import 'package:app/ui/pages/pdf/pdf_page.dart';
import 'package:app/ui/pages/post/post_binding.dart';
import 'package:app/ui/pages/post/post_page.dart';
import 'package:app/ui/pages/splash/splash_binding.dart';
import 'package:app/ui/pages/splash/splash_page.dart';
import 'package:app/ui/pages/tabs/tabs_binding.dart';
import 'package:app/ui/pages/tabs/tabs_page.dart';
import 'package:get/get.dart';

class AppRoutes {
  AppRoutes._();

  static const String initialPath = SplashPage.routeName;

  static final routes = [
    GetPage(
      name: SplashPage.routeName,
      page: () => const SplashPage(),
      binding: SplashBinding(),
    ),
    GetPage(
      name: TabsPage.routeName,
      page: () => const TabsPage(),
      binding: TabsBinding(),
    ),
    GetPage(
      name: LoginPage.routeName,
      page: () => const LoginPage(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: PaymentsPage.routeName,
      page: () => const PaymentsPage(),
      binding: PaymentsBinding(),
    ),
    GetPage(
      name: PaymentPage.routeName,
      page: () => PaymentPage(),
      binding: PaymentBinding(),
    ),
    GetPage(
      name: CategoryPage.routeName,
      page: () => const CategoryPage(),
      binding: CategoryBinding(),
    ),
    GetPage(
      name: PostPage.routeName,
      page: () => const PostPage(),
      binding: PostBinding(),
    ),
    GetPage(
      name: PdfPage.routeName,
      page: () => const PdfPage(),
      binding: PdfBinding(),
    ),
  ];
}
