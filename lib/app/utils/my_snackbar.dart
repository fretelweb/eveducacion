import 'package:flutter/material.dart';
import 'package:get/get.dart';

enum MySnackBarType { error, success }

void mySnackBar(
  String title,
  String message, {
  MySnackBarType type = MySnackBarType.success,
}) {
  Get.snackbar(
    title,
    message,
    backgroundColor: type == MySnackBarType.success ? Colors.green : Colors.red,
    colorText: Colors.white,
  );
}
