import 'package:app/app/constants/app_constants.dart';

baseUrl(String path) {
  if (path.startsWith('http')) {
    return path;
  }
  if (!path.startsWith('/')) {
    path = '/$path';
  }

  return '${AppConstants.apiBaseUrl}$path';
}
