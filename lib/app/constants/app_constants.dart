class AppConstants {
  static const String appName = 'Sunshine';
  static const String appVersion = '1.0.0';
  static const String appBuildNumber = '1';
  static const String apiBaseUrl = 'https://eveducacion.enlacevisual'
      '.cl/sunshine';
}
